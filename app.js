
const express = require('express');
const bodyParser = require('body-parser');
const translate = require('./translate');

const app = express();

app.use(bodyParser.json());

app.get('/', (req, res) => {
    res.status(503).json({ message: 'Service Unavailable' });
});

app.post('/translate', async (req, res) => {
    try {
        console.log(req.body);
        const {from, to, text} = req.body;
        const result = await translate(from, to, text);
        console.log(result);
        res.status(result.error ? 400 : 200).json(result);
    } catch (error) {
        res.status(400).json(error);
    }
});

if (module === require.main) {
    const server = app.listen(process.env.PORT || 9000, () => {
        const port = server.address().port;
        console.log(`App listening on port ${port}`);
    });
}

module.exports = app;