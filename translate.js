const projectId = 'twitch-api-viewer';
const location = 'global';

const { TranslationServiceClient } = require('@google-cloud/translate').v3beta1;
const keyFilename = './Twitch Naivebot-3718dd703056.json'

const translationClient = new TranslationServiceClient({ projectId, keyFilename });

async function translateText(sourceLanguageCode, targetLanguageCode, text) {
    const request = {
        parent: translationClient.locationPath(projectId, location),
        contents: [text],
        mimeType: 'text/plain', // mime types: text/plain, text/html
        sourceLanguageCode: sourceLanguageCode,
        targetLanguageCode: targetLanguageCode,
    };

    try {
        const [response] = await translationClient.translateText(request);

        /*for (const translation of response.translations) {
            console.log(`Translation: ${translation.translatedText}`);
        }*/
    
        return response.translations; 
    } catch (error) {
        return {error};
    }
}

module.exports = translateText;